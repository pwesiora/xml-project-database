<?xml version="1.0" encoding="UTF-8"?>
<xs:stylesheet version="2.0" xmlns:xs="http://www.w3.org/1999/XSL/Transform">
<xs:mode use-accumulators="variable" streamable="no"/>
    <xs:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />


            </head>

            <body>
                <div class="container">
                    <h1>Użytkownicy(łącznie:<xs:value-of select="count(liga/adresy/adres)"  />)</h1>
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Id uzytkownika</th>
                                <th scope="col">Imie</th>
                                <th scope="col">Nazwisko</th>
                                <th scope="col">Kraj</th>
                                <th scope="col">Prowincja</th>
                                <th scope="col">Miasto</th>
                                <th scope="col">Ulica</th>
                                <th scope="col">Numer</th>
                                <th scope="col">Kod Pocztowy</th>
                            </tr>
                        </thead>
                        <tbody>
                               <xs:for-each select="liga/uzytkownicy/uzytkownik">
                                <xs:sort select="liga/uzytkownicy/uzytkownik/@id_uz"/>
<xs:variable name="adr_var" > 
<xs:value-of select="@id_adres"/>
</xs:variable>

<tr>
<td><xs:value-of select="@id_uz" /></td>
                                            <td><xs:value-of select="imie" /></td>
                                            <td><xs:value-of select="nazwisko" /></td>
<xs:for-each select="/liga/adresy/adres[@id_adres=$adr_var]">


<xs:choose>
<xs:when test="string-length(kraj) &gt; 1">
                                            <td><xs:value-of select="kraj" /></td>
                                            <td><xs:value-of select="prowincja" /></td>
                                            <td><xs:value-of select="miasto" /></td>
<td><xs:value-of select="ulica" /></td>
<td><xs:value-of select="numer" /></td>
<td><xs:value-of select="kod-pocztowy" /></td>
  </xs:when>
  <xs:otherwise>
    <td>---</td>
    <td>---</td>
    <td>---</td>
<td>---</td>
<td>---</td>
<td>---</td>
  </xs:otherwise>
</xs:choose> 

                            </xs:for-each>
</tr>
                           </xs:for-each>

                        </tbody>
                    </table>

                    <h1>Gry</h1>

                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Data rozpoczecia</th>
                                <th scope="col">Data zakonczenia</th>
								<th scope="col">Gracz bialy</th>
								<th scope="col">Gracz czarny</th>
                                <th scope="col">Wynik</th>
                                <th scope="col">Turniej</th>
                            </tr>
                        </thead>
                        <tbody>

                            <xs:for-each select="liga/gry/gra">
<xs:variable name="turn_var" > 
<xs:value-of select="@turniej"/>
</xs:variable>
<xs:variable name="gB_var" > 
<xs:value-of select="@gracz_bialy"/>
</xs:variable>
<xs:variable name="gC_var" > 
<xs:value-of select="@gracz_czarny"/>
</xs:variable>
                                <tr>
                                    <td><xs:value-of select="data_rozpoczecia" /></td>
                                    <td><xs:value-of select="data_zakonczenia" /></td>
							<xs:for-each select="/liga/uzytkownicy/uzytkownik[@id_uz=$gB_var]">
                                    <td><xs:value-of select="imie" />  <xs:text> </xs:text><xs:value-of select="nazwisko" /></td>
                            </xs:for-each>
							<xs:for-each select="/liga/uzytkownicy/uzytkownik[@id_uz=$gC_var]">
                                    <td><xs:value-of select="imie" />  <xs:text> </xs:text><xs:value-of select="nazwisko" /></td>
                            </xs:for-each>

<td><xs:value-of select="wynik" /></td>
<xs:for-each select="/liga/turnieje/turniej[@id_turniej!=$turn_var]">
                                    <td>---</td>
                            </xs:for-each>
<xs:for-each select="/liga/turnieje/turniej[@id_turniej=$turn_var]">
                                    <td><xs:value-of select="nazwa" /></td>
                            </xs:for-each>


                                </tr>
                            </xs:for-each>a
                        </tbody>
                    </table>
                    <h1>Turnieje</h1>

                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Nazwa</th>
                                <th scope="col">Opis</th>
                                <th scope="col">Data Rozpoczecia</th>
                                <th scope="col">Data Zakonczenia</th>
                            </tr>
                        </thead>
                        <tbody>

                            <xs:for-each select="liga/turnieje/turniej">

                                <tr>
                                    <td><xs:value-of select="nazwa" /></td>
                                    <td><xs:value-of select="opis" /></td>
                                    <td><xs:value-of select="data_rozpoczecia" /></td>
                                    <td><xs:value-of select="data_zakonczenia" /></td>
                                </tr>
                            </xs:for-each>
                        </tbody>
                    </table>
                </div>
            </body>
        </html>
    </xs:template>
</xs:stylesheet>